FROM node:22-alpine3.19

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY . .
RUN ls -l
EXPOSE 9000

CMD ["npm", "start:dev"]