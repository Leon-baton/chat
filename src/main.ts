import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        cors: true,
        bodyParser: true,
    });
    const configService: ConfigService<unknown, boolean> = app.get(ConfigService);
    const appConfig = configService.get('app');

    app.useGlobalPipes(new ValidationPipe());
    await app.listen(appConfig.port);

    return appConfig;
}

bootstrap().then((config) => {
    Logger.log(`Сервер запущен http://${config.baseURL}:${config.port}/`, 'bootstrap');
});
