import { Controller, Post, Body } from '@nestjs/common';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { FindByIdDto } from './dto/find-chat.dto';
import { Message } from './entities/message.entity';

@Controller('messages')
export class MessageController {
    constructor(private readonly messageService: MessageService) {}

    @Post('add')
    async create(@Body() createMessageDto: CreateMessageDto) {
        return await this.messageService.create(createMessageDto);
    }

    @Post('get')
    async findAll(@Body() findByIdDto: FindByIdDto):Promise<Message[]> {
        return await this.messageService.findAll(findByIdDto);
    }
}
