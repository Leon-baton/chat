import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

export class FindByIdDto {
    @IsNumber()
    @IsPositive()
    @IsNotEmpty()
    chat: number;
}
