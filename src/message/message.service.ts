import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Message } from './entities/message.entity';
import { Repository } from 'typeorm';
import { FindByIdDto } from './dto/find-chat.dto';

@Injectable()
export class MessageService {
    constructor(
        @InjectRepository(Message) private readonly messageRepository: Repository<Message>,
    ) {}

    async create(createMessageDto: CreateMessageDto): Promise<number> {
        const message = this.messageRepository.create(createMessageDto);
        const { id } = await this.messageRepository.save(message);
        return id;
    }

    async findAll(findByIdDto: FindByIdDto): Promise<Message[]> {
        const { chat } = findByIdDto;
        const messages = await this.messageRepository.find({ 
            where: {
                chat: {
                    id: chat
                }
            },
            relations: ['author', 'chat'],
            order: {
                create_at: 'ASC'
            }
        });

        return messages;
    }
}
