import { User } from '../../user/entities/user.entity';
import { Chat } from '../../chat/entities/chat.entity';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Message {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    text: string;

    @ManyToOne(() => Chat, (chat) => chat.messages)
    chat: Chat;

    @ManyToOne(() => User, (user) => user.id)
    author: User;

    @CreateDateColumn()
    create_at: Date;
}
