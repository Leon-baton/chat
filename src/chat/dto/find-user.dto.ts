import { IsNotEmpty, IsNumber, IsPositive } from "class-validator";

export class FindUserDto {
    @IsNotEmpty()
    @IsNumber()
    @IsPositive()
    user: number;
}