import { Injectable } from '@nestjs/common';
import { CreateChatDto } from './dto/create-chat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Chat } from './entities/chat.entity';
import { Repository, In } from 'typeorm';
import { User } from 'src/user/entities/user.entity';
import { FindUserDto } from './dto/find-user.dto';

@Injectable()
export class ChatService {
    constructor(
        @InjectRepository(Chat) private readonly chatRepository: Repository<Chat>,
        @InjectRepository(User) private readonly userRepository: Repository<User>
    ) {}

    async create(createChatDto: CreateChatDto) {
        const users = await this.userRepository.findBy({ id: In(createChatDto.users) });

        if (users.length != createChatDto.users.length) {
            throw new Error('Некоторые пользователи не были найдены.');
        }

        const chat = this.chatRepository.create({ name: createChatDto.name, users });
        const { id } = await this.chatRepository.save(chat);

        return id;
    }

    async findBy(findUserDto: FindUserDto):Promise<Chat[]> {
        const { user } = findUserDto;
        const chats = await this.chatRepository.find({
            where: { users: { id: user } },
            relations: ['messages'],
        });
      
        chats.sort((a, b) => {
            return a.messages[a.messages.length - 1]?.create_at.getTime() - b.messages[b.messages.length - 1]?.create_at.getTime();
        });

        return chats;
    }
}
