import { Message } from '../../message/entities/message.entity';
import { User } from '../../user/entities/user.entity';
import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Chat {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(() => User, (user) => user.chats)
    @JoinTable()
    users: User[];

    @CreateDateColumn()
    create_at: Date;

    @OneToMany(() => Message, (message) => message.chat)
    messages: Message[];
}
