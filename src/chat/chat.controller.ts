import { Controller, Post, Body } from '@nestjs/common';
import { ChatService } from './chat.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { FindUserDto } from './dto/find-user.dto';

@Controller('chats')
export class ChatController {
    constructor(private readonly chatService: ChatService) {}

    @Post('add')
    async create(@Body() createChatDto: CreateChatDto) {
        return await this.chatService.create(createChatDto);
    }

    @Post('get')
    async findBy(@Body() findUserDto: FindUserDto) {
        return await this.chatService.findBy(findUserDto);
    }
}
